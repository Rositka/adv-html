const perfData = window.performance.timing;

const interactive = perfData.domInteractive - perfData.domLoading;
const domContentLoaded = perfData.domContentLoadedEventStart - perfData.domLoading;
const complete = perfData.domComplete - perfData.domLoading;

console.log('interactive: ' + interactive + 'ms, ' +
    'dom content loaded: ' + domContentLoaded + 'ms, complete: ' + complete + 'ms');

const pageLoadTime = perfData.loadEventEnd - perfData.navigationStart;
console.log(`Load time page ${pageLoadTime}`);

const connectTime = perfData.responseEnd - perfData.requestStart;
console.log(`Time to connect ${connectTime}`);

const renderTime = perfData.domComplete - perfData.domLoading;
console.log(`Time to render ${renderTime}`);


console.log(perfData);